const secondHand = document.querySelector(".sec-hand");
const minHand = document.querySelector(".min-hand");
const hourHand = document.querySelector(".hour-hand");

function setDate() {
    const now = new Date();
    const sec = now.getSeconds();
    const secondDegrees = ((sec / 60) * 360) + 90;
    secondHand.style.transform = `rotate(${secondDegrees}deg)`;
    secondHand.style.backgroundColor = `#00528b`;
    secondHand.style.height = "2px";
    // secondHand.style.left = "77px";


    const min = now.getMinutes();
    const minDegrees = ((min / 60) * 360) + 90;
    minHand.style.transform = `rotate(${minDegrees}deg)`;


    const hour = now.getHours();
    const hourDegrees = ((hour / 12) * 360) + ((min/60)*30) + 90;
    hourHand.style.transform = `rotate(${hourDegrees}deg)`;
    hourHand.style.width = "150px";
    hourHand.style.left = "76px";
}

setInterval(setDate, 1000);